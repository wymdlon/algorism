package sort;

public class InsertionSort {

	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = insertionSort(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] insertionSort(int [] mas) {
		for(int i = 0; i < mas.length; i++) {
			for(int j = i; j > 0 && mas[j - 1] > mas[j]; j--) {
				int temp = mas[j];
				mas[j] = mas[j - 1];
				mas[j - 1] = temp;
			}
		}
		return mas;
	}
	

}
