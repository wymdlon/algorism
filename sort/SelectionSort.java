package sort;

public class SelectionSort {
	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = selectionSort(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] selectionSort(int [] mas) {
		for(int i = 0; i < mas.length - 1; i++) {
			int indexMin = i;
			for(int j = i + 1; j < mas.length; j++) {
				if(mas[j] < mas[indexMin]) {
					indexMin = j;
				}
			}
			int temp = mas[i];
			mas[i] = mas[indexMin];
			mas[indexMin] = temp;
		}
		return mas;
	}
}
