package sort;

public class InsertionSortMIT {

	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = insertionSort(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] insertionSort(int [] mas) {
		for(int i = 1; i < mas.length; i++) {
			int temp = mas[i];
			int j;
			for(j = i - 1; j >= 0 && mas[j] > temp; j--) {
				mas[j + 1] = mas[j];
			}
			mas[j + 1] = temp;
		}
		
		return mas;
	}
}
