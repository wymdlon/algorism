package sort;

public class BubbleSortbBidirectional {
	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = bubbleSortBiderectional(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] bubbleSortBiderectional(int [] mas) {
		int l = 0;
		int r = mas.length - 1;
		boolean swapped = true;
		do {
			swapped = false;
			for (int i = l; i < r; i++) {
				if (mas[i + 1] < mas[i]) {
					swap(mas, i, i + 1);
					swapped = true;
				}
			}
			r--;
			for (int i = r; i > l; i--) {
				if (mas[i] < mas[i - 1]) {
					swap(mas, i, i - 1);
					swapped = true;
				}
			}
			l++;
	
		} while (l < r && swapped);
		
		return mas;
	}
	public static int [] swap(int [] mas, int a, int b) {
		
		int temp = mas[a];
		mas[a] = mas[b];
		mas[b] = temp;
		
		return mas;
	}
}
