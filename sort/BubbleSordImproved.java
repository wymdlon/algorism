package sort;

public class BubbleSordImproved {
	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = bubbleSort(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] bubbleSort(int [] mas) {
		boolean swapped = true;
		for(int i = 0; i < mas.length - 1 && swapped; i++) {
			swapped = false;
			for(int j = 0; j < mas.length - 1; j++) {
				if(mas[j] > mas[j + 1]) {
					int temp = mas[j];
					mas[j] = mas[j + 1];
					mas[j + 1] = temp;
					swapped = true;
				}
			}
			
		}
		return mas;
	}
}
