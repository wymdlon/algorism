package sort;

public class ShellSort {
	public static void main(String[] args) {
		int [] mass = {21, 2, 18, 10, 43, 18, 24};
		mass = shellSort(mass);
		for(int i = 0; i < mass.length; i++) {
			System.out.print(mass[i] + " ");
		}
	}
	
	public static int [] shellSort(int [] mas) {
		
		int h = 1;
		while(h <= mas.length / 3) {
			h = h * 3 + 1;
		}
		
		while(h > 0) {
			for(int i = h; i < mas.length; i++) {
				int temp = mas[i];
				int j = i;
				for(; j > h - 1 && temp <= mas[j - h]; j -= h) {
					mas[j] = mas[j - h];
				}
				mas[j] = temp;
			} 
			
			h /= 3;
		}
		return mas;
	}
}
