package search;

public class ExponentialSearch {
	
	public static void main(String[] args) {
		int [] mass = {2, 55, 55, 55, 55 , 65, 85};
		int key = 65;
		int searchIndex = exponentialSearch(mass, key);
		System.out.println(searchIndex);
	}
	
	public static int binarySearch(int [] mass, int key, int lo, int hi) {
		while(lo <= hi) {
			int mid = (lo + hi) / 2;
			if(mass[mid] == key) {
				return mid;
			}
			else if(key > mass[mid]) {
				lo = mid + 1;
			}
			else hi = mid - 1;
		}
		return -1;
	}
	public static int exponentialSearch(int [] mas, int key) {
		int bount = 1;
		int len = mas.length;
		
		while(bount < len && mas[bount] < key) {
			bount = bount << 1;
			if(bount < len && mas[bount] == key) {
				return bount;
			}
		}
		
		int lowerBount = bount >>> 1;
		int upperBount = Math.min(len - 1, bount);
		
		return binarySearch(mas, key, lowerBount, upperBount);
	}
}
