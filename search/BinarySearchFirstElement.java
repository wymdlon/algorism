package search;

public class BinarySearchFirstElement {

	public static void main(String[] args) {
		int [] mass = {2, 22, 55, 55, 55 , 65, 85};
		int key = 55;
		System.out.println(binarySearch(mass, key));
	}
	public static int binarySearch(int [] mass, int key) {
		int lo = 0;
		int hi = mass.length - 1;
		while(lo <= hi) {
			int mid = (lo + hi) / 2;
			if((mid == 0 || key > mass[mid - 1]) && mass[mid] == key) {
				return mid;
			}
			else if(key > mass[mid]) {
				lo = mid + 1;
			}
			else hi = mid - 1;
		}
		return -1;
	}
}
