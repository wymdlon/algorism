package search;

public class InterpolationSearch {
	public static void main(String[] args) {
		int [] mass = {2, 4, 6, 10, 12, 18, 24};
		int key = 24;
		int searchIndex = interpolationSearch(mass, key);
		System.out.println(searchIndex);
	}
	public static int interpolationSearch(int [] mas, int key) {
		int lo = 0;
		int hi = mas.length - 1;
		while(lo <= hi) {
			int mid = lo + (key - mas[lo])*(hi - lo)/(mas[hi] - mas[lo]);
			if(mas[mid] == key) {
				return mid;
			}
			else if (key > mas[mid]) {
				lo = mid + 1;
			}
			else hi = mid - 1;
		}
		return -1;
	}
}	
