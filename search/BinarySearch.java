package search;

public class BinarySearch {

	public static void main(String[] args) {
		int [] mass = {2, 4, 6, 10, 12, 18, 24};
		int key = 24;
		System.out.println(binarySearch(mass, key));
	}
	public static int binarySearch(int [] mass, int key) {
		int lo = 0;
		int hi = mass.length - 1;
		while(lo <= hi) {
			int mid = (lo + hi) / 2;
			if(mass[mid] == key) {
				return mid;
			}
			else if(key > mass[mid]) {
				lo = mid + 1;
			}
			else hi = mid - 1;
		}
		return -1;
	}
}
